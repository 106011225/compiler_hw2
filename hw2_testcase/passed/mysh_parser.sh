
for f in *.txt; do
    NAME_ANS=${f/.txt/.ans};
    NAME_OUT=${f/.txt/.out};
    golden_parser < $f > $NAME_ANS;
    ../../parser < $f > $NAME_OUT;
    diff $NAME_ANS $NAME_OUT -s;
done

