#define LITERAL_NUM 257
#define LITERAL_DOUBLE 258
#define IDENTIFIER 259
#define LITERAL_CHAR 260
#define LITERAL_STRING 261
#define CONST 262
#define SIGNED 263
#define UNSIGNED 264
#define LONG 265
#define SHORT 266
#define INT 267
#define CHAR 268
#define FLOAT 269
#define DOUBLE 270
#define VOID 271
#define OP_EQ 272
#define OP_NE 273
#define OP_LE 274
#define OP_GE 275
#define OP_SHIFT_L 276
#define OP_SHIFT_R 277
#define OP_LOGICAL_OR 278
#define OP_LOGICAL_AND 279
#define OP_INC 280
#define OP_DEC 281
#define BREAK 282
#define CONTINUE 283
#define RETURN 284
#define IF 285
#define ELSE 286
#define SWITCH 287
#define CASE 288
#define DEFAULT 289
#define WHILE 290
#define DO 291
#define FOR 292
#define ADDRESS_OF 293
#define DEREFERENCE 294
#define UPLUS 295
#define UMINUS 296
#define SUFFIX_INC 297
#define SUFFIX_DEC 298
#ifdef YYSTYPE
#undef  YYSTYPE_IS_DECLARED
#define YYSTYPE_IS_DECLARED 1
#endif
#ifndef YYSTYPE_IS_DECLARED
#define YYSTYPE_IS_DECLARED 1
typedef union{
    int intVal;
    double dVal;
    char cVal;
    nodeInfo node;
} YYSTYPE;
#endif /* !YYSTYPE_IS_DECLARED */
extern YYSTYPE yylval;
