%{

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "nodeInfo.h"

// add this to avoid warning: implicit declaration of function ‘yylex’ [-Wimplicit-function-declaration]
int yylex(void);

nodeInfo ndArr[20];
char buf[301];

nodeInfo ch2nd(char c);
nodeInfo concatChild(int numChild, char* tagType);

%}

%union{
    int intVal;
    double dVal;
    char cVal;
    nodeInfo node;
}


%start start

%token<intVal> LITERAL_NUM 
%token<dVal> LITERAL_DOUBLE 
%token<cVal> ';' ':' ',' '=' '*' '[' ']' '{' '}' '(' ')' '|' '^' '&' '<' '>' '+' '-' '/' '%' '!' '~' 
%token<node> IDENTIFIER LITERAL_CHAR LITERAL_STRING
%token<node> CONST SIGNED UNSIGNED LONG SHORT INT CHAR FLOAT DOUBLE VOID 
%token<node> OP_EQ OP_NE OP_LE OP_GE OP_SHIFT_L OP_SHIFT_R OP_LOGICAL_OR OP_LOGICAL_AND OP_INC OP_DEC
%token<node> BREAK CONTINUE RETURN IF ELSE SWITCH CASE DEFAULT WHILE DO FOR 

%type<node> start global_item_list global_item scalar_decl array_decl func_decl func_def
%type<node> type ident_list type_specifier_list type_specifier iden_initORnot scalar_pointerORnot 
%type<node> array_list array_initORnot arr_size arr_content arr_content_list 
%type<node> parameter_list parameter stmt_or_vardecl_list 
%type<node> expr_assignment_list expr_assignment expr_logical_or expr_logical_and 
%type<node> expr_bitwise_or expr_bitwise_xor expr_bitwise_and expr_equality expr_relational expr_shift 
%type<node> expr_additive expr_multiplicative expr_cast expr_unary expr_postfix expr_primary variable literal 
%type<node> stmt if_stmt switch_stmt while_stmt for_stmt return_stmt compound_stmt switch_clause_list switch_clause 


%left ','
%right '='
%left OP_LOGICAL_OR
%left OP_LOGICAL_AND
%left '|'
%left '^' 
%left '&'
%left OP_EQ OP_NE
%left OP_LE OP_GE '<' '>'
%left OP_SHIFT_L OP_SHIFT_R
%left '+' '-'
%left '*' '/' '%'
%right ADDRESS_OF 
%right DEREFERENCE 
%right'!' '~'
%right UPLUS UMINUS 
%right OP_INC OP_DEC // prefix
%left SUFFIX_INC SUFFIX_DEC // suffix 
%left '[' ']'
%left '(' ')'



%%


////////////////////////////////// begin global

start 
    : global_item_list {ndArr[0] = $1; printf("%s", ndArr[0].str); free(ndArr[0].str);}
    /* : global_item_list {ndArr[0] = $1; $$ = concatChild(1, NULL);} */
    ;

global_item_list
    : global_item_list global_item {ndArr[0] = $1; ndArr[1] = $2; $$ = concatChild(2, NULL);}
    | global_item {ndArr[0] = $1; $$ = concatChild(1, NULL);}
    ;

global_item
    : scalar_decl {ndArr[0] = $1; $$ = concatChild(1, NULL);}
    | array_decl {ndArr[0] = $1; $$ = concatChild(1, NULL);}
    | func_decl {ndArr[0] = $1; $$ = concatChild(1, NULL);}
    | func_def {ndArr[0] = $1; $$ = concatChild(1, NULL);}
    ;

///////////////////////////////// end global begin scalar

scalar_decl
    : type ident_list ';' {ndArr[0] = $1; ndArr[1] = $2; ndArr[2] = ch2nd($3); $$ = concatChild(3, "sdc");}
    ;

type
    : CONST type_specifier_list {ndArr[0] = $1; ndArr[1] = $2; $$ = concatChild(2, NULL);}
    | type_specifier_list  {ndArr[0] = $1; $$ = concatChild(1, NULL); }
    | CONST  {ndArr[0] = $1; $$ = concatChild(1, NULL);}
    ;

type_specifier_list
    : type_specifier_list type_specifier {ndArr[0] = $1; ndArr[1] = $2; $$ = concatChild(2, NULL);}
    | type_specifier {ndArr[0] = $1; $$ = concatChild(1, NULL);} 
    ;

type_specifier
    : SIGNED {ndArr[0] = $1; $$ = concatChild(1, NULL);}
    | UNSIGNED {ndArr[0] = $1; $$ = concatChild(1, NULL);}
    | LONG {ndArr[0] = $1; $$ = concatChild(1, NULL);}
    | SHORT {ndArr[0] = $1; $$ = concatChild(1, NULL);}
    | INT  {ndArr[0] = $1; $$ = concatChild(1, NULL); }
    | CHAR {ndArr[0] = $1; $$ = concatChild(1, NULL);}
    | FLOAT {ndArr[0] = $1; $$ = concatChild(1, NULL);}
    | DOUBLE {ndArr[0] = $1; $$ = concatChild(1, NULL);}
    | VOID {ndArr[0] = $1; $$ = concatChild(1, NULL);}
    ;

ident_list
    : ident_list ',' iden_initORnot {ndArr[0] = $1; ndArr[1] = ch2nd($2); ndArr[2] = $3; $$ = concatChild(3, NULL);}
    | iden_initORnot  {ndArr[0] = $1; $$ = concatChild(1, NULL); }
    ;

iden_initORnot
    : scalar_pointerORnot {ndArr[0] = $1; $$ = concatChild(1, NULL);}
    | scalar_pointerORnot '=' expr_assignment_list {ndArr[0] = $1; ndArr[1] = ch2nd($2); ndArr[2] = $3; $$ = concatChild(3, NULL);}
    ;

scalar_pointerORnot
    : IDENTIFIER {ndArr[0] = $1; $$ = concatChild(1, NULL); }
    | '*' IDENTIFIER %prec DEREFERENCE {ndArr[0] = ch2nd($1); ndArr[1] = $2; $$ = concatChild(2, NULL);}
    ;


/////////////////////////// end scalar begin array_decl
    
array_decl
    : type array_list ';' {ndArr[0] = $1; ndArr[1] = $2; ndArr[2] = ch2nd($3); $$ = concatChild(3, "adc");}
    ;

array_list
    : array_list ',' array_initORnot {ndArr[0] = $1; ndArr[1] = ch2nd($2); ndArr[2] = $3; $$ = concatChild(3, NULL);}
    | array_initORnot {ndArr[0] = $1; $$ = concatChild(1, NULL);}
    ;
    
array_initORnot
    : IDENTIFIER arr_size {ndArr[0] = $1; ndArr[1] = $2; $$ = concatChild(2, NULL);}
    | IDENTIFIER arr_size '=' arr_content {ndArr[0] = $1; ndArr[1] = $2; ndArr[2] = ch2nd($3); ndArr[3] = $4; $$ = concatChild(4, NULL);}
    ;

arr_size
    : arr_size '[' expr_assignment_list ']' {ndArr[0] = $1; ndArr[1] = ch2nd($2); ndArr[2] = $3; ndArr[3] = ch2nd($4); $$ = concatChild(4, NULL);}
    | '[' expr_assignment_list ']' {ndArr[0] = ch2nd($1); ndArr[1] = $2; ndArr[2] = ch2nd($3);  $$ = concatChild(3, NULL);}
    ;

arr_content
    : '{' arr_content_list '}' {ndArr[0] = ch2nd($1); ndArr[1] = $2; ndArr[2] = ch2nd($3);  $$ = concatChild(3, NULL);}
    | '{' expr_assignment_list '}' {ndArr[0] = ch2nd($1); ndArr[1] = $2; ndArr[2] = ch2nd($3);  $$ = concatChild(3, NULL);}
    ;

arr_content_list
    : arr_content_list ',' arr_content {ndArr[0] = $1; ndArr[1] = ch2nd($2); ndArr[2] = $3; $$ = concatChild(3, NULL);}
    | arr_content {ndArr[0] = $1; $$ = concatChild(1, NULL);}
    ;

/////////////////////////// end array_decl begin func_decl 

func_decl
    : type scalar_pointerORnot '(' ')' ';'  {ndArr[0] = $1; ndArr[1] = $2; ndArr[2] = ch2nd($3); ndArr[3] = ch2nd($4); ndArr[4] = ch2nd($5); $$ = concatChild(5, "fdc");}
    | type scalar_pointerORnot '(' parameter_list ')' ';' {ndArr[0] = $1; ndArr[1] = $2; ndArr[2] = ch2nd($3); ndArr[3] = $4; ndArr[4] = ch2nd($5); ndArr[5] = ch2nd($6); $$ = concatChild(6, "fdc");}
    ;

parameter_list
    : parameter_list ',' parameter {ndArr[0] = $1; ndArr[1] = ch2nd($2); ndArr[2] = $3; $$ = concatChild(3, NULL);}
    | parameter {ndArr[0] = $1; $$ = concatChild(1, NULL);}
    ;

parameter
    : type scalar_pointerORnot  {ndArr[0] = $1; ndArr[1] = $2; $$ = concatChild(2, NULL);}
    ;

/////////////////////////// end func_decl begin func_def

func_def
    : type scalar_pointerORnot '(' ')' '{' stmt_or_vardecl_list '}' {ndArr[0] = $1; ndArr[1] = $2; ndArr[2] = ch2nd($3); ndArr[3] = ch2nd($4); ndArr[4] = ch2nd($5); ndArr[5] = $6; ndArr[6] = ch2nd($7); $$ = concatChild(7, "fdf");}
    | type scalar_pointerORnot '(' parameter_list ')' '{' stmt_or_vardecl_list '}' {ndArr[0] = $1; ndArr[1] = $2; ndArr[2] = ch2nd($3); ndArr[3] = $4; ndArr[4] = ch2nd($5); ndArr[5] = ch2nd($6); ndArr[6] = $7; ndArr[7] = ch2nd($8); $$ = concatChild(8, "fdf");}
    ;

/////////////////////////// end func_def begin expr 


// precedence rank 15 (precedence of operator)
expr_assignment_list 
    : expr_assignment_list ',' expr_assignment {ndArr[0] = $1; ndArr[1] = ch2nd($2); ndArr[2] = $3; $$ = concatChild(3, NULL);}
    | expr_assignment {ndArr[0] = $1; $$ = concatChild(1, NULL);}
    ;

// precedence rank 14
expr_assignment
    : expr_unary '=' expr_assignment {ndArr[0] = $1; ndArr[1] = ch2nd($2); ndArr[2] = $3; $$ = concatChild(3, "expr");}
    | expr_logical_or {ndArr[0] = $1; $$ = concatChild(1, NULL);}
    ;

// precedence rank 12 (13 is not used)
expr_logical_or
    : expr_logical_or OP_LOGICAL_OR expr_logical_and  {ndArr[0] = $1; ndArr[1] = $2; ndArr[2] = $3; $$ = concatChild(3, "expr");}
    | expr_logical_and {ndArr[0] = $1; $$ = concatChild(1, NULL);}
    ;

// precedence rank 11
expr_logical_and
    : expr_logical_and OP_LOGICAL_AND expr_bitwise_or {ndArr[0] = $1; ndArr[1] = $2; ndArr[2] = $3; $$ = concatChild(3, "expr");}
    | expr_bitwise_or {ndArr[0] = $1; $$ = concatChild(1, NULL);}
    ;

// precedence rank 10
expr_bitwise_or
    : expr_bitwise_or '|' expr_bitwise_xor {ndArr[0] = $1; ndArr[1] = ch2nd($2); ndArr[2] = $3; $$ = concatChild(3, "expr");}
    | expr_bitwise_xor {ndArr[0] = $1; $$ = concatChild(1, NULL);}
    ;

// precedence rank 9
expr_bitwise_xor
    : expr_bitwise_xor '^' expr_bitwise_and {ndArr[0] = $1; ndArr[1] = ch2nd($2); ndArr[2] = $3; $$ = concatChild(3, "expr");}
    | expr_bitwise_and {ndArr[0] = $1; $$ = concatChild(1, NULL);}
    ;

// precedence rank 8
expr_bitwise_and
    : expr_bitwise_and '&' expr_equality {ndArr[0] = $1; ndArr[1] = ch2nd($2); ndArr[2] = $3; $$ = concatChild(3, "expr");}
    | expr_equality {ndArr[0] = $1; $$ = concatChild(1, NULL);}
    ;

// precedence rank 7
expr_equality
    : expr_equality OP_EQ expr_relational {ndArr[0] = $1; ndArr[1] = $2; ndArr[2] = $3; $$ = concatChild(3, "expr");}
    | expr_equality OP_NE expr_relational {ndArr[0] = $1; ndArr[1] = $2; ndArr[2] = $3; $$ = concatChild(3, "expr");}
    | expr_relational {ndArr[0] = $1; $$ = concatChild(1, NULL);}
    ;

// precedence rank 6
expr_relational
    : expr_relational '<' expr_shift {ndArr[0] = $1; ndArr[1] = ch2nd($2); ndArr[2] = $3; $$ = concatChild(3, "expr");}
    | expr_relational OP_LE expr_shift {ndArr[0] = $1; ndArr[1] = $2; ndArr[2] = $3; $$ = concatChild(3, "expr");}
    | expr_relational '>' expr_shift {ndArr[0] = $1; ndArr[1] = ch2nd($2); ndArr[2] = $3; $$ = concatChild(3, "expr");}
    | expr_relational OP_GE expr_shift {ndArr[0] = $1; ndArr[1] = $2; ndArr[2] = $3; $$ = concatChild(3, "expr");}
    | expr_shift {ndArr[0] = $1; $$ = concatChild(1, NULL);}
    ;

// precedence rank 5
expr_shift
    : expr_shift OP_SHIFT_L expr_additive {ndArr[0] = $1; ndArr[1] = $2; ndArr[2] = $3; $$ = concatChild(3, "expr");}
    | expr_shift OP_SHIFT_R expr_additive {ndArr[0] = $1; ndArr[1] = $2; ndArr[2] = $3; $$ = concatChild(3, "expr");}
    | expr_additive {ndArr[0] = $1; $$ = concatChild(1, NULL);}
    ;

// precedence rank 4
expr_additive
    : expr_additive '+' expr_multiplicative {ndArr[0] = $1; ndArr[1] = ch2nd($2); ndArr[2] = $3; $$ = concatChild(3, "expr");}
    | expr_additive '-' expr_multiplicative {ndArr[0] = $1; ndArr[1] = ch2nd($2); ndArr[2] = $3; $$ = concatChild(3, "expr");}
    | expr_multiplicative {ndArr[0] = $1; $$ = concatChild(1, NULL);}
    ;

// precedence rank 3
expr_multiplicative
    : expr_multiplicative '*' expr_cast {ndArr[0] = $1; ndArr[1] = ch2nd($2); ndArr[2] = $3; $$ = concatChild(3, "expr");}
    | expr_multiplicative '/' expr_cast {ndArr[0] = $1; ndArr[1] = ch2nd($2); ndArr[2] = $3; $$ = concatChild(3, "expr");}
    | expr_multiplicative '%' expr_cast {ndArr[0] = $1; ndArr[1] = ch2nd($2); ndArr[2] = $3; $$ = concatChild(3, "expr");}
    | expr_cast {ndArr[0] = $1; $$ = concatChild(1, NULL);}
    ;

// precedence rank 2 (The operand of prefix ++ and -- can't be a type cast)
expr_cast
    : '(' type ')' expr_cast {ndArr[0] = ch2nd($1); ndArr[1] = $2; ndArr[2] = ch2nd($3); ndArr[3] = $4; $$ = concatChild(4, "expr");}
    | '(' type '*' ')' expr_cast {ndArr[0] = ch2nd($1); ndArr[1] = $2; ndArr[2] = ch2nd($3); ndArr[3] = ch2nd($4); ndArr[4] = $5; $$ = concatChild(5, "expr");}
    | expr_unary {ndArr[0] = $1; $$ = concatChild(1, NULL);}
    ;

// precedence rank 2
expr_unary
    : '&' expr_cast %prec ADDRESS_OF {ndArr[0] = ch2nd($1); ndArr[1] = $2; $$ = concatChild(2, "expr");}
    | '*' expr_cast %prec DEREFERENCE {ndArr[0] = ch2nd($1); ndArr[1] = $2; $$ = concatChild(2, "expr");}
    | '!' expr_cast {ndArr[0] = ch2nd($1); ndArr[1] = $2; $$ = concatChild(2, "expr");}
    | '~' expr_cast {ndArr[0] = ch2nd($1); ndArr[1] = $2; $$ = concatChild(2, "expr");}
    | '+' expr_cast %prec UPLUS {ndArr[0] = ch2nd($1); ndArr[1] = $2; $$ = concatChild(2, "expr");}
    | '-' expr_cast %prec UMINUS {ndArr[0] = ch2nd($1); ndArr[1] = $2; $$ = concatChild(2, "expr");}
    | OP_INC expr_unary {ndArr[0] = $1; ndArr[1] = $2; $$ = concatChild(2, "expr");}
    | OP_DEC expr_unary {ndArr[0] = $1; ndArr[1] = $2; $$ = concatChild(2, "expr");}
    | expr_postfix {ndArr[0] = $1; $$ = concatChild(1, NULL);}
    ;

// precedence rank 1
expr_postfix
    : expr_postfix '(' ')' {ndArr[0] = $1; ndArr[1] = ch2nd($2); ndArr[2] = ch2nd($3); $$ = concatChild(3, "expr");}
    | expr_postfix '(' expr_assignment_list ')' {ndArr[0] = $1; ndArr[1] = ch2nd($2); ndArr[2] = $3; ndArr[3] = ch2nd($4); $$ = concatChild(4, "expr");}
//    | expr_postfix '[' expr_assignment_list ']' {ndArr[0] = $1; ndArr[1] = ch2nd($2); ndArr[2] = $3; ndArr[3] = ch2nd($4); $$ = concatChild(4, "expr");}
    | expr_postfix OP_INC %prec SUFFIX_INC {ndArr[0] = $1; ndArr[1] = $2; $$ = concatChild(2, "expr");}
    | expr_postfix OP_DEC %prec SUFFIX_DEC {ndArr[0] = $1; ndArr[1] = $2; $$ = concatChild(2, "expr");}
    | expr_primary {ndArr[0] = $1; $$ = concatChild(1, NULL);}
    ;

// parentheses dictate a new precedence squence for the enclosed "expr"
expr_primary
    : variable {ndArr[0] = $1; $$ = concatChild(1, "expr");}
    | literal {ndArr[0] = $1; $$ = concatChild(1, "expr");}
    | '(' expr_assignment_list ')' {ndArr[0] = ch2nd($1); ndArr[1] = $2; ndArr[2] = ch2nd($3); $$ = concatChild(3, "expr");}
    ;

variable
    : IDENTIFIER  {ndArr[0] = $1; $$ = concatChild(1, NULL);}
    | IDENTIFIER arr_size  {ndArr[0] = $1; ndArr[1] = $2; $$ = concatChild(2, NULL);}
    ;

literal
    : LITERAL_NUM {sprintf(buf, "%d", $1); ndArr[0].len = strlen(buf); ndArr[0].str = strdup(buf); $$ = concatChild(1, NULL);}
    | LITERAL_DOUBLE {sprintf(buf, "%f", $1); ndArr[0].len = strlen(buf); ndArr[0].str = strdup(buf); $$ = concatChild(1, NULL);}
    | LITERAL_CHAR  {ndArr[0] = $1; $$ = concatChild(1, NULL);}
    | LITERAL_STRING  {ndArr[0] = $1; $$ = concatChild(1, NULL);}
    ;

/////////////////////////// end expr begin stmt 
    
stmt
    : expr_assignment_list ';'  {ndArr[0] = $1; ndArr[1] = ch2nd($2); $$ = concatChild(2, "stmt");}
    | if_stmt  {ndArr[0] = $1; $$ = concatChild(1, NULL);}
    | switch_stmt  {ndArr[0] = $1; $$ = concatChild(1, NULL);}
    | while_stmt  {ndArr[0] = $1; $$ = concatChild(1, NULL);}
    | for_stmt  {ndArr[0] = $1; $$ = concatChild(1, NULL);}
    | return_stmt  {ndArr[0] = $1; $$ = concatChild(1, NULL);}
    | BREAK ';'  {ndArr[0] = $1; ndArr[1] = ch2nd($2); $$ = concatChild(2, "stmt");}
    | CONTINUE ';'  {ndArr[0] = $1; ndArr[1] = ch2nd($2); $$ = concatChild(2, "stmt");}
    | compound_stmt  {ndArr[0] = $1; $$ = concatChild(1, NULL);}
    ;

if_stmt
    : IF '(' expr_assignment_list ')' '{' '}' {ndArr[0] = $1; ndArr[1] = ch2nd($2); ndArr[2] = $3; ndArr[3] = ch2nd($4); ndArr[4] = ch2nd($5); ndArr[5] = ch2nd($6); $$ = concatChild(6, "stmt");}
    | IF '(' expr_assignment_list ')' '{' stmt_or_vardecl_list '}' {ndArr[0] = $1; ndArr[1] = ch2nd($2); ndArr[2] = $3; ndArr[3] = ch2nd($4); ndArr[4] = ch2nd($5); ndArr[5] = $6; ndArr[6] = ch2nd($7); $$ = concatChild(7, "stmt");}
    | IF '(' expr_assignment_list ')' '{' '}' ELSE '{' '}' {ndArr[0] = $1; ndArr[1] = ch2nd($2); ndArr[2] = $3; ndArr[3] = ch2nd($4); ndArr[4] = ch2nd($5); ndArr[5] = ch2nd($6); ndArr[6] = $7; ndArr[7] = ch2nd($8); ndArr[8] = ch2nd($9); $$ = concatChild(9, "stmt");}
    | IF '(' expr_assignment_list ')' '{' stmt_or_vardecl_list '}' ELSE '{' '}' {ndArr[0] = $1; ndArr[1] = ch2nd($2); ndArr[2] = $3; ndArr[3] = ch2nd($4); ndArr[4] = ch2nd($5); ndArr[5] = $6; ndArr[6] = ch2nd($7); ndArr[7] = $8; ndArr[8] = ch2nd($9); ndArr[9] = ch2nd($10); $$ = concatChild(10, "stmt");}
    | IF '(' expr_assignment_list ')' '{' '}' ELSE '{' stmt_or_vardecl_list '}' {ndArr[0] = $1; ndArr[1] = ch2nd($2); ndArr[2] = $3; ndArr[3] = ch2nd($4); ndArr[4] = ch2nd($5); ndArr[5] = ch2nd($6); ndArr[6] = $7; ndArr[7] = ch2nd($8); ndArr[8] = $9; ndArr[9] = ch2nd($10); $$ = concatChild(10, "stmt");}
    | IF '(' expr_assignment_list ')' '{' stmt_or_vardecl_list '}' ELSE '{' stmt_or_vardecl_list '}' {ndArr[0] = $1; ndArr[1] = ch2nd($2); ndArr[2] = $3; ndArr[3] = ch2nd($4); ndArr[4] = ch2nd($5); ndArr[5] = $6; ndArr[6] = ch2nd($7); ndArr[7] = $8; ndArr[8] = ch2nd($9); ndArr[9] = $10; ndArr[10] = ch2nd($11); $$ = concatChild(11, "stmt");}
    ;


switch_stmt
    : SWITCH '(' expr_assignment_list ')' '{' switch_clause_list '}' {ndArr[0] = $1; ndArr[1] = ch2nd($2); ndArr[2] = $3; ndArr[3] = ch2nd($4); ndArr[4] = ch2nd($5); ndArr[5] = $6; ndArr[6] = ch2nd($7); $$ = concatChild(7, "stmt");}
    | SWITCH '(' expr_assignment_list ')' '{' '}' {ndArr[0] = $1; ndArr[1] = ch2nd($2); ndArr[2] = $3; ndArr[3] = ch2nd($4); ndArr[4] = ch2nd($5); ndArr[5] = ch2nd($6); $$ = concatChild(6, "stmt");}
    ;

switch_clause_list
    : switch_clause_list switch_clause {ndArr[0] = $1; ndArr[1] = $2; $$ = concatChild(2, NULL);}
    | switch_clause {ndArr[0] = $1; $$ = concatChild(1, NULL);}
    ;

switch_clause
    : CASE expr_assignment_list ':' stmt_or_vardecl_list {ndArr[0] = $1; ndArr[1] = $2; ndArr[2] = ch2nd($3); ndArr[3] = $4; $$ = concatChild(4, NULL);}
    | DEFAULT ':' stmt_or_vardecl_list {ndArr[0] = $1; ndArr[1] = ch2nd($2); ndArr[2] = $3; $$ = concatChild(3, NULL);}
    ;


while_stmt
    : WHILE '(' expr_assignment_list ')' stmt {ndArr[0] = $1; ndArr[1] = ch2nd($2); ndArr[2] = $3; ndArr[3] = ch2nd($4); ndArr[4] = $5; $$ = concatChild(5, "stmt");}
    | DO stmt WHILE '(' expr_assignment_list ')' ';' {ndArr[0] = $1; ndArr[1] = $2; ndArr[2] = $3; ndArr[3] = ch2nd($4); ndArr[4] = $5; ndArr[5] = ch2nd($6); ndArr[6] = ch2nd($7); $$ = concatChild(7, "stmt");}
    ;

for_stmt
    : FOR '(' expr_assignment_list ';' expr_assignment_list ';' expr_assignment_list ')' stmt {ndArr[0] = $1; ndArr[1] = ch2nd($2); ndArr[2] = $3; ndArr[3] = ch2nd($4); ndArr[4] = $5; ndArr[5] = ch2nd($6); ndArr[6] = $7; ndArr[7] = ch2nd($8); ndArr[8] = $9; $$ = concatChild(9, "stmt");}
    | FOR '(' ';' expr_assignment_list ';' expr_assignment_list ')' stmt {ndArr[0] = $1; ndArr[1] = ch2nd($2); ndArr[2] = ch2nd($3); ndArr[3] = $4; ndArr[4] = ch2nd($5); ndArr[5] = $6; ndArr[6] = ch2nd($7); ndArr[7] = $8; $$ = concatChild(8, "stmt");}
    | FOR '(' expr_assignment_list ';'  ';' expr_assignment_list ')' stmt {ndArr[0] = $1; ndArr[1] = ch2nd($2); ndArr[2] = $3; ndArr[3] = ch2nd($4); ndArr[4] = ch2nd($5); ndArr[5] = $6; ndArr[6] = ch2nd($7); ndArr[7] = $8; $$ = concatChild(8, "stmt");}
    | FOR '(' expr_assignment_list ';' expr_assignment_list ';'  ')' stmt {ndArr[0] = $1; ndArr[1] = ch2nd($2); ndArr[2] = $3; ndArr[3] = ch2nd($4); ndArr[4] = $5; ndArr[5] = ch2nd($6); ndArr[6] = ch2nd($7); ndArr[7] = $8; $$ = concatChild(8, "stmt");}
    | FOR '(' ';'  ';' expr_assignment_list ')' stmt {ndArr[0] = $1; ndArr[1] = ch2nd($2); ndArr[2] = ch2nd($3); ndArr[3] = ch2nd($4); ndArr[4] = $5; ndArr[5] = ch2nd($6); ndArr[6] = $7; $$ = concatChild(7, "stmt");}
    | FOR '(' ';' expr_assignment_list ';' ')' stmt {ndArr[0] = $1; ndArr[1] = ch2nd($2); ndArr[2] = ch2nd($3); ndArr[3] = $4; ndArr[4] = ch2nd($5); ndArr[5] = ch2nd($6); ndArr[6] = $7; $$ = concatChild(7, "stmt");}
    | FOR '(' expr_assignment_list ';' ';' ')' stmt {ndArr[0] = $1; ndArr[1] = ch2nd($2); ndArr[2] = $3; ndArr[3] = ch2nd($4); ndArr[4] = ch2nd($5); ndArr[5] = ch2nd($6); ndArr[6] = $7; $$ = concatChild(7, "stmt");}
    | FOR '(' ';' ';' ')' stmt {ndArr[0] = $1; ndArr[1] = ch2nd($2); ndArr[2] = ch2nd($3); ndArr[3] = ch2nd($4); ndArr[4] = ch2nd($5); ndArr[5] = $6; $$ = concatChild(6, "stmt");}
    ;

return_stmt
    : RETURN expr_assignment_list ';'  {ndArr[0] = $1; ndArr[1] = $2; ndArr[2] = ch2nd($3); $$ = concatChild(3, "stmt");}
    | RETURN ';' {ndArr[0] = $1; ndArr[1] = ch2nd($2); $$ = concatChild(2, "stmt");}
    ;

compound_stmt
    : '{' '}' {ndArr[0] = ch2nd($1); ndArr[1] = ch2nd($2); $$ = concatChild(2, "stmt");}
    | '{' stmt_or_vardecl_list '}' {ndArr[0] = ch2nd($1); ndArr[1] = $2; ndArr[2] = ch2nd($3); $$ = concatChild(3, "stmt");}
    ;

stmt_or_vardecl_list
    : stmt_or_vardecl_list stmt {ndArr[0] = $1; ndArr[1] = $2; $$ = concatChild(2, NULL);}
    | stmt_or_vardecl_list scalar_decl {ndArr[0] = $1; ndArr[1] = $2; $$ = concatChild(2, NULL);}
    | stmt_or_vardecl_list array_decl {ndArr[0] = $1; ndArr[1] = $2; $$ = concatChild(2, NULL);}
    | stmt {ndArr[0] = $1; $$ = concatChild(1, NULL);}
    | scalar_decl {ndArr[0] = $1; $$ = concatChild(1, NULL);}
    | array_decl {ndArr[0] = $1; $$ = concatChild(1, NULL);}
    ;


///////////////////////// end stmt




%%

int main(void){
    yyparse();
    return 0;
}

void yyerror(char* msg){
    fprintf(stderr, "%s\n", msg);
    exit(1);
}

nodeInfo ch2nd(char c){
    char str[2];
    sprintf(str, "%c", c);

    nodeInfo retVal;
    retVal.len = 1;
    retVal.str = strdup(str);
    
    return retVal;
}

nodeInfo concatChild(int numChild, char* tagType){
    int i; 
    nodeInfo retVal;
    retVal.len = 0;

    
    for (i = 0; i < numChild; i++) 
        retVal.len += ndArr[i].len;        

    if (tagType == NULL){
        retVal.str = (char*)malloc((retVal.len+1)*sizeof(char));
        *(retVal.str) = '\0';
        for (i = 0; i < numChild; i++) {
            strcat(retVal.str, ndArr[i].str);
            free(ndArr[i].str);
        }
    }
    else if (!strcmp(tagType, "sdc")){
        retVal.len += 27; // tag length
        retVal.str = (char*)malloc((retVal.len+1)*sizeof(char));
        *(retVal.str) = '\0';
        strcat(retVal.str, "<scalar_decl>");
        for (i = 0; i < numChild; i++) {
            strcat(retVal.str, ndArr[i].str);
            free(ndArr[i].str);
        }
        strcat(retVal.str, "</scalar_decl>");
    }
    else if (!strcmp(tagType, "adc")){
        retVal.len += 25; // tag length
        retVal.str = (char*)malloc((retVal.len+1)*sizeof(char));
        *(retVal.str) = '\0';
        strcat(retVal.str, "<array_decl>");
        for (i = 0; i < numChild; i++) {
            strcat(retVal.str, ndArr[i].str);
            free(ndArr[i].str);
        }
        strcat(retVal.str, "</array_decl>");
    }
    else if (!strcmp(tagType, "fdc")){
        retVal.len += 23; // tag length
        retVal.str = (char*)malloc((retVal.len+1)*sizeof(char));
        *(retVal.str) = '\0';
        strcat(retVal.str, "<func_decl>");
        for (i = 0; i < numChild; i++) {
            strcat(retVal.str, ndArr[i].str);
            free(ndArr[i].str);
        }
        strcat(retVal.str, "</func_decl>");
    }
    else if (!strcmp(tagType, "fdf")){
        retVal.len += 21; // tag length
        retVal.str = (char*)malloc((retVal.len+1)*sizeof(char));
        *(retVal.str) = '\0';
        strcat(retVal.str, "<func_def>");
        for (i = 0; i < numChild; i++) {
            strcat(retVal.str, ndArr[i].str);
            free(ndArr[i].str);
        }
        strcat(retVal.str, "</func_def>");
    }
    else if (!strcmp(tagType, "expr")){
        retVal.len += 13; // tag length
        retVal.str = (char*)malloc((retVal.len+1)*sizeof(char));
        *(retVal.str) = '\0';
        strcat(retVal.str, "<expr>");
        for (i = 0; i < numChild; i++) {
            strcat(retVal.str, ndArr[i].str);
            free(ndArr[i].str);
        }
        strcat(retVal.str, "</expr>");
    }
    else if (!strcmp(tagType, "stmt")){
        retVal.len += 13; // tag length
        retVal.str = (char*)malloc((retVal.len+1)*sizeof(char));
        *(retVal.str) = '\0';
        strcat(retVal.str, "<stmt>");
        for (i = 0; i < numChild; i++) {
            strcat(retVal.str, ndArr[i].str);
            free(ndArr[i].str);
        }
        strcat(retVal.str, "</stmt>");
    }
    
    return retVal;
}



