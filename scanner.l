%{
#include "nodeInfo.h"

#include "y.tab.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>



char strbuf[301];

nodeInfo tmp;

%}

%x STATE_STRING
%x STATE_COMMENT

%%

" "+ {}
\t+ {}
\n {}

"for" {tmp.len = strlen(yytext); tmp.str = strdup(yytext); yylval.node = tmp; return FOR;}
"do" {tmp.len = strlen(yytext); tmp.str = strdup(yytext); yylval.node = tmp; return DO;}
"while" {tmp.len = strlen(yytext); tmp.str = strdup(yytext); yylval.node = tmp; return WHILE;}
"break" {tmp.len = strlen(yytext); tmp.str = strdup(yytext); yylval.node = tmp;return BREAK;}
"continue" {tmp.len = strlen(yytext); tmp.str = strdup(yytext); yylval.node = tmp; return CONTINUE;}
"if" {tmp.len = strlen(yytext); tmp.str = strdup(yytext); yylval.node = tmp; return IF;}
"else" {tmp.len = strlen(yytext); tmp.str = strdup(yytext); yylval.node = tmp; return ELSE;}
"return" {tmp.len = strlen(yytext); tmp.str = strdup(yytext); yylval.node = tmp; return RETURN;}
"switch" {tmp.len = strlen(yytext); tmp.str = strdup(yytext); yylval.node = tmp; return SWITCH;}
"case" {tmp.len = strlen(yytext); tmp.str = strdup(yytext); yylval.node = tmp; return CASE;}
"default" {tmp.len = strlen(yytext); tmp.str = strdup(yytext); yylval.node = tmp; return DEFAULT;}
"void" {tmp.len = strlen(yytext); tmp.str = strdup(yytext); yylval.node = tmp; return VOID;}
"int" {tmp.len = strlen(yytext); tmp.str = strdup(yytext); yylval.node = tmp;return INT;}
"double" {tmp.len = strlen(yytext); tmp.str = strdup(yytext); yylval.node = tmp; return DOUBLE;}
"float" {tmp.len = strlen(yytext); tmp.str = strdup(yytext); yylval.node = tmp; return FLOAT;}
"char" {tmp.len = strlen(yytext); tmp.str = strdup(yytext); yylval.node = tmp; return CHAR;}
"const" {tmp.len = strlen(yytext); tmp.str = strdup(yytext); yylval.node = tmp;return CONST;}
"signed" {tmp.len = strlen(yytext); tmp.str = strdup(yytext); yylval.node = tmp; return SIGNED;}
"unsigned" {tmp.len = strlen(yytext); tmp.str = strdup(yytext); yylval.node = tmp; return UNSIGNED;}
"short" {tmp.len = strlen(yytext); tmp.str = strdup(yytext); yylval.node = tmp; return SHORT;}
"long" {tmp.len = strlen(yytext); tmp.str = strdup(yytext); yylval.node = tmp; return LONG;}

"NULL" {yylval.intVal = 0; return LITERAL_NUM;}

[_[:alpha:]][_[:alnum:]]* {tmp.len = strlen(yytext); tmp.str = strdup(yytext); yylval.node = tmp; return IDENTIFIER;}

"+" {yylval.cVal = yytext[0]; return '+';}
"-" {yylval.cVal = yytext[0]; return '-';}
"*" {yylval.cVal = yytext[0]; return '*';}
"/" {yylval.cVal = yytext[0]; return '/';}
"%" {yylval.cVal = yytext[0]; return '%';}
"++" {tmp.len = strlen(yytext); tmp.str = strdup(yytext); yylval.node = tmp; return OP_INC;}
"--" {tmp.len = strlen(yytext); tmp.str = strdup(yytext); yylval.node = tmp; return OP_DEC;}
"<" {yylval.cVal = yytext[0]; return '<';}
"<=" {tmp.len = strlen(yytext); tmp.str = strdup(yytext); yylval.node = tmp; return OP_LE;}
">" {yylval.cVal = yytext[0]; return '>';}
">=" {tmp.len = strlen(yytext); tmp.str = strdup(yytext); yylval.node = tmp; return OP_GE;}
"==" {tmp.len = strlen(yytext); tmp.str = strdup(yytext); yylval.node = tmp; return OP_EQ;}
"!=" {tmp.len = strlen(yytext); tmp.str = strdup(yytext); yylval.node = tmp; return OP_NE;}
"=" {yylval.cVal = yytext[0]; return '=';}
"&&" {tmp.len = strlen(yytext); tmp.str = strdup(yytext); yylval.node = tmp; return OP_LOGICAL_AND;}
"||" {tmp.len = strlen(yytext); tmp.str = strdup(yytext); yylval.node = tmp; return OP_LOGICAL_OR;}
"!" {yylval.cVal = yytext[0]; return '!';}
"~" {yylval.cVal = yytext[0]; return '~';}
"^" {yylval.cVal = yytext[0]; return '^';}
"&" {yylval.cVal = yytext[0]; return '&';}
"|" {yylval.cVal = yytext[0]; return '|';}

"<<" {tmp.len = strlen(yytext); tmp.str = strdup(yytext); yylval.node = tmp; return OP_SHIFT_L;}
">>" {tmp.len = strlen(yytext); tmp.str = strdup(yytext); yylval.node = tmp; return OP_SHIFT_R;}

":" {yylval.cVal = yytext[0]; return ':';}
";" {yylval.cVal = yytext[0]; return ';';}
"," {yylval.cVal = yytext[0]; return ',';}
"." {yylval.cVal = yytext[0]; return '.';}
"[" {yylval.cVal = yytext[0]; return '[';}
"]" {yylval.cVal = yytext[0]; return ']';}
"(" {yylval.cVal = yytext[0]; return '(';}
")" {yylval.cVal = yytext[0]; return ')';}
"{" {yylval.cVal = yytext[0]; return '{';}
"}" {yylval.cVal = yytext[0]; return '}';}

[[:digit:]]+ {yylval.intVal = atoi(yytext); return LITERAL_NUM;}

(([0-9]*[.][0-9]+)|([0-9]+[.][0-9]*)) {yylval.dVal = atof(yytext); return LITERAL_DOUBLE;}



\" {BEGIN STATE_STRING; strcpy(strbuf, "\"");} 
<STATE_STRING>\\[abefnrtv0] {strcat(strbuf, yytext);}
<STATE_STRING>\\\\ {strcat(strbuf, yytext);}
<STATE_STRING>\\' {strcat(strbuf, yytext);}
<STATE_STRING>\\\" {strcat(strbuf, yytext);}
<STATE_STRING>\\\? {strcat(strbuf, yytext);}
<STATE_STRING>\\[0-7]{1,3} {strcat(strbuf, yytext);}
<STATE_STRING>\\x[0-9A-Fa-f]+ {strcat(strbuf, yytext);}
<STATE_STRING>\\u[0-9A-Fa-f]{4,4} {strcat(strbuf, yytext);}
<STATE_STRING>\\U[0-9A-Fa-f]{8,8} {strcat(strbuf, yytext);}

<STATE_STRING>\" {BEGIN(INITIAL); strcat(strbuf, yytext); tmp.len = strlen(strbuf); tmp.str = strdup(strbuf); yylval.node = tmp; return LITERAL_STRING;}

<STATE_STRING>. {strcat(strbuf, yytext);}


'\\[abefnrtv0]' {tmp.len = strlen(yytext); tmp.str = strdup(yytext); yylval.node = tmp; return LITERAL_CHAR;}
'\\\\' {tmp.len = strlen(yytext); tmp.str = strdup(yytext); yylval.node = tmp; return LITERAL_CHAR;}
'\\'' {tmp.len = strlen(yytext); tmp.str = strdup(yytext); yylval.node = tmp; return LITERAL_CHAR;}
'\\\"' {tmp.len = strlen(yytext); tmp.str = strdup(yytext); yylval.node = tmp; return LITERAL_CHAR;}
'\\\?' {tmp.len = strlen(yytext); tmp.str = strdup(yytext); yylval.node = tmp; return LITERAL_CHAR;}
'\\[0-7]{1,3}' {tmp.len = strlen(yytext); tmp.str = strdup(yytext); yylval.node = tmp; return LITERAL_CHAR;}
'\\x[0-9A-Fa-f]+' {tmp.len = strlen(yytext); tmp.str = strdup(yytext); yylval.node = tmp; return LITERAL_CHAR;}
'\\u[0-9A-Fa-f]{4,4}' {tmp.len = strlen(yytext); tmp.str = strdup(yytext); yylval.node = tmp; return LITERAL_CHAR;}
'\\U[0-9A-Fa-f]{8,8}' {tmp.len = strlen(yytext); tmp.str = strdup(yytext); yylval.node = tmp; return LITERAL_CHAR;}
'.' {tmp.len = strlen(yytext); tmp.str = strdup(yytext); yylval.node = tmp; return LITERAL_CHAR;}



\/\/.*\n {}

"/*" {BEGIN STATE_COMMENT;}
<STATE_COMMENT>. {}
<STATE_COMMENT>\n {}
<STATE_COMMENT>"*/" {BEGIN(INITIAL);}


%%

// eliminate warning of unused variables warning...
int (*input_funcPointer)(void) = input;
void (*yyunput_funcPointer)(int, char*) = yyunput;
