# Compiler assignments: Syntax analyzer with `Yacc`


## What is `Yacc`?

- A tool which can produce a parser with a given grammar
- A program designed to compile a LALR grammar and produce the source code of the syntactic analyzer of the language defined by this grammar 


## Interface between `Lex` and `Yacc`

- The interface is `y.tab.h`, which is produced by `Yacc`.
- How to create `y.tab.h` and use it?
    - `$ yacc -d parser.y`
        - The command will produce `y.tab.h` and `y.tab.c`.
    - Include `y.tab.h` in the Lex program.


## Goal 

- Generate a syntax analyzer for the subset of C language
    - Please refer to [SPEC.pdf](./SPEC.pdf) **page 30** for more details.


[Input example](./test/sample_input_hw2.c):
```c
int main() {
  printf("%p\n", NULL);
  return 0;
}
```

[Output example](./test/sample_input_hw2.ans):
```xml
<func_def>intmain(){<stmt><expr><expr>printf</expr>(<expr>"%p\n"</expr>,<expr>0</expr>)</expr>;</stmt><stmt>return<expr>0</expr>;</stmt>}</func_def>
```

Formatted output:
```xml
<func_def>intmain(){
	<stmt>
		<expr>
			<expr>printf</expr>(
			<expr>"%p\n"</expr>,
			<expr>0</expr>)
		</expr>;
	</stmt>
	<stmt>return
		<expr>0</expr>;
	</stmt>}
</func_def>
```


## Usage

1. Generate the source code of `parser`
    ```
    $ flex scanner.l
    $ byacc -d -v parser.y
    ```

2. Compile the source code of `parser`
    ```
    $ gcc lex.yy.c y.tab.c -o parser -lfl
    ```

Alternatively, use `$ make` to replace step 1. & 2.  

3. Test the `parser` program
    ```
    $ ./parser < test.c
    ```


**Please refer to [SPEC.pdf](./SPEC.pdf) for more details.**
